import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { IdCardComponent } from './id-card/id-card.component';
import { SkillsComponent } from './skills/skills.component';
import { ExperienceComponent } from './experience/experience.component';
import { FormationComponent } from './formation/formation.component';
import { ContainerMainPartComponent } from './container-main-part/container-main-part.component';
import { RandomFactsComponent } from './random-facts/random-facts.component';

@NgModule({
  declarations: [
    AppComponent,
    IdCardComponent,
    SkillsComponent,
    ExperienceComponent,
    FormationComponent,
    ContainerMainPartComponent,
    RandomFactsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
