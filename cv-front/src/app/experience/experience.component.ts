import { Component, OnInit } from '@angular/core';

import { ContainerMainPartComponent } from '../container-main-part/container-main-part.component';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.css']
})
export class ExperienceComponent implements OnInit {

  constructor(private containerMainPartComponent: ContainerMainPartComponent) { }

  ngOnInit() {
    document.getElementsByClassName("current-component")[0].classList.remove("current-component");
    document.getElementById("nav-exp").classList.add("current-component");

    this.maxHeight();

    window.onresize = (e) => {
      this.maxHeight();
      let firstExperience = document.getElementsByClassName("experience")[0].firstChild as HTMLElement;
      firstExperience.click();
    }

    let firstExperience = document.getElementsByClassName("experience")[0].firstChild as HTMLElement;
    firstExperience.click();

  }

  maxHeight(): void{
    let experiences = Array.from(document.getElementsByClassName("experience") as HTMLCollectionOf<HTMLElement>);
    for(let iExp = 0; iExp < experiences.length; iExp++){
      experiences[iExp].classList.remove("inactive");
    }

    let expBodies = Array.from(document.getElementsByClassName("exp-body") as HTMLCollectionOf<HTMLElement>);
    for(const expBody of expBodies){
      expBody.style.maxHeight = 'auto';
      expBody.style.height = 'auto';

      expBody.style.maxHeight = expBody.clientHeight + 5 + "px";
      expBody.style.height = expBody.clientHeight + 5 + "px";
    }

    this.hideAll();
  }
  hideAll(): void{
    let experiences = Array.from(document.getElementsByClassName("experience") as HTMLCollectionOf<HTMLElement>);
    for(let iExp = 0; iExp < experiences.length; iExp++){
      if(!experiences[iExp].classList.contains("inactive")){
        let expHead = experiences[iExp].childNodes[0] as HTMLElement;
        expHead.click();
      }
    }
  }

  activate(event):void{
    if(event.target.parentNode.classList.contains("inactive")){
      this.hideAll();
      event.target.nextSibling.style.height = event.target.nextSibling.style.maxHeight;
      //switch the image
      this.containerMainPartComponent.setName(event.target.id);
    }
    else{
      event.target.nextSibling.style.height = "0px";
      this.containerMainPartComponent.setName("");
    }
    event.target.parentNode.classList.toggle("inactive");
  }

}
