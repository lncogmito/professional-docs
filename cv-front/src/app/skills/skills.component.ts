import { Component, OnInit } from '@angular/core';

import { ContainerMainPartComponent } from '../container-main-part/container-main-part.component';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit {
  current: number;
  private skillContainer: HTMLElement;

  constructor(private containerMainPartComponent: ContainerMainPartComponent) { }

  ngOnInit() {
    document.getElementsByClassName("current-component")[0].classList.remove("current-component");
    document.getElementById("nav-skills").classList.add("current-component");

    this.skillContainer = document.getElementById("skills-container");

    this.maxHeight();
    let firstSkill = document.getElementsByClassName("skill")[0].firstChild as HTMLElement;
    firstSkill.click();

    window.onresize = (e) => {
      this.maxHeight();
      let firstSkill = document.getElementsByClassName("skill")[0].firstChild as HTMLElement;
      firstSkill.click();
    }
  }

  maxHeight(): void{
    /* Those two lines splitted in the beginning and the end of this function if you want to  */
//    this.skillContainer.style.visibility = "hidden";
//    setTimeout(() => {this.skillContainer.style.visibility = "visible";}, 600);
    let skills = Array.from(document.getElementsByClassName("skill") as HTMLCollectionOf<HTMLElement>);
    for(let iSkill = 0; iSkill < skills.length; iSkill++){
      skills[iSkill].classList.remove("inactive");
    }

    let skillBodies = Array.from(document.getElementsByClassName("skill-body") as HTMLCollectionOf<HTMLElement>);
    for(const skillBody of skillBodies){
      /* Make sure all are opened */
      skillBody.style.maxHeight = 'auto';
      skillBody.style.height = 'auto';

      skillBody.style.maxHeight = skillBody.clientHeight + 5 + "px";
      skillBody.style.height = skillBody.clientHeight + 5 + "px";
    }

    this.hideAll();
  }
  hideAll(): void{
    let skills = Array.from(document.getElementsByClassName("skill") as HTMLCollectionOf<HTMLElement>);
    for(let iSkill = 0; iSkill < skills.length; iSkill++){
      if(!skills[iSkill].classList.contains("inactive")){
        let skillHead = skills[iSkill].childNodes[0] as HTMLElement;
        skillHead.click();
      }
    }
  }

  activate(event):void{
    if(event.target.parentNode.classList.contains("inactive")){
      this.hideAll();
      event.target.nextSibling.style.height = event.target.nextSibling.style.maxHeight;
      //switch the image
      this.containerMainPartComponent.setName(event.target.id);
    }
    else{
      event.target.nextSibling.style.height = "0px";
      this.containerMainPartComponent.setName("");
    }
    event.target.parentNode.classList.toggle("inactive");
  }

  expand(event): void{
    event.target.classList.toggle("active");
    event.target.nextSibling.classList.toggle("inactive");
  }

}
