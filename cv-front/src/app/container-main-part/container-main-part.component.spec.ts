import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerMainPartComponent } from './container-main-part.component';

describe('ContainerMainPartComponent', () => {
  let component: ContainerMainPartComponent;
  let fixture: ComponentFixture<ContainerMainPartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerMainPartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerMainPartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
