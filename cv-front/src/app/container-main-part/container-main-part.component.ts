import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-container-main-part',
  templateUrl: './container-main-part.component.html',
  styleUrls: ['./container-main-part.component.css']
})
export class ContainerMainPartComponent implements OnInit {
  private name: string;
  constructor() { }

  ngOnInit() {
  }

  setName(newName: string): void{
    this.name = newName;
  }

}
