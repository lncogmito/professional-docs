import { Component, OnInit } from '@angular/core';

import { ContainerMainPartComponent } from '../container-main-part/container-main-part.component';

@Component({
  selector: 'app-random-facts',
  templateUrl: './random-facts.component.html',
  styleUrls: ['./random-facts.component.css']
})
export class RandomFactsComponent implements OnInit {

  constructor(private containerMainPartComponent: ContainerMainPartComponent) { }

  ngOnInit() {
    document.getElementsByClassName("current-component")[0].classList.remove("current-component");
    document.getElementById("nav-random").classList.add("current-component");

    this.containerMainPartComponent.setName('');
  }
  expand(event): void{
    event.target.classList.toggle("active");
    event.target.nextSibling.classList.toggle("inactive");
  }

}
