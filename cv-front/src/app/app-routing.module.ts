import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExperienceComponent } from './experience/experience.component';
import { FormationComponent } from './formation/formation.component';
import { RandomFactsComponent } from './random-facts/random-facts.component';
import { SkillsComponent } from './skills/skills.component';


const routes: Routes = [
  { path: '', redirectTo: '/experiences', pathMatch: 'full'},
  { path: 'experiences', component: ExperienceComponent },
  { path: 'formations', component: FormationComponent },
  { path: 'divers', component: RandomFactsComponent },
  { path: 'skills', component: SkillsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
