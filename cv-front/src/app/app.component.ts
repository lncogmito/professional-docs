import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private router:Router){}

  ngOnInit(){
    document.getElementById("profile-container").style.height = window.innerHeight+"px";

    let contentHeight = window.innerHeight - 90;
    document.getElementById("data-container").style.height = contentHeight+"px";
    document.getElementById("content-container").style.height = contentHeight+"px";
  }

  onResize($event): void{
    document.getElementById("profile-container").style.height = window.innerHeight+"px";

    let contentHeight = window.innerHeight - 90;
    document.getElementById("data-container").style.height = contentHeight+"px";
    document.getElementById("content-container").style.height = contentHeight+"px";

    if(window.innerWidth < 1200){
      console.log("Redirection car écran trop petit");
      // mettre une redirection propre vers la partie mobile <3
      // et vérifier que ce critère soit suffisant pour détecter les écrans mobiles
      //document.getElementById("google").click();
    }
  }
}
