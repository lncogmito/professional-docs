import { Component, OnInit } from '@angular/core';

import { ContainerMainPartComponent } from '../container-main-part/container-main-part.component';

@Component({
  selector: 'app-formation',
  templateUrl: './formation.component.html',
  styleUrls: ['./formation.component.css']
})
export class FormationComponent implements OnInit {

  constructor(private containerMainPartComponent: ContainerMainPartComponent) { }

  ngOnInit() {
    document.getElementsByClassName("current-component")[0].classList.remove("current-component");
    document.getElementById("nav-forma").classList.add("current-component");

    this.maxHeight();
    let firstFormation = document.getElementsByClassName("formation")[0].firstChild as HTMLElement;
    firstFormation.click();

    window.onresize = (e) => {
      this.maxHeight();
      let firstFormation = document.getElementsByClassName("formation")[0].firstChild as HTMLElement;
      firstFormation.click();
    }
  }

  maxHeight(): void{
    let formations = Array.from(document.getElementsByClassName("formation") as HTMLCollectionOf<HTMLElement>);
    for(let iForma = 0; iForma < formations.length; iForma++){
      formations[iForma].classList.remove("inactive");
    }

    let formaBodies = Array.from(document.getElementsByClassName("forma-body") as HTMLCollectionOf<HTMLElement>);
    for(const formaBody of formaBodies){
      /* Make sure all are opened */
      formaBody.style.maxHeight = 'auto';
      formaBody.style.height = 'auto';

      formaBody.style.maxHeight = formaBody.clientHeight + 5 + "px";
      formaBody.style.height = formaBody.clientHeight + 5 + "px";
    }

    this.hideAll();
  }
  hideAll(): void{
    let formations = Array.from(document.getElementsByClassName("formation") as HTMLCollectionOf<HTMLElement>);
    for(let iForma = 0; iForma < formations.length; iForma++){
      if(!formations[iForma].classList.contains("inactive")){
        let formaHead = formations[iForma].childNodes[0] as HTMLElement;
        formaHead.click();
      }
    }
  }

  activate(event):void{
    if(event.target.parentNode.classList.contains("inactive")){
      this.hideAll();
      event.target.nextSibling.style.height = event.target.nextSibling.style.maxHeight;
      //switch the image
      this.containerMainPartComponent.setName(event.target.id);
    }
    else{
      event.target.nextSibling.style.height = "0px";
      this.containerMainPartComponent.setName("");
    }
    event.target.parentNode.classList.toggle("inactive");
  }

}
